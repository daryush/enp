<?php

namespace spec\Acme;

use Acme\ImageFinder;
use Acme\ImagesData;
use Acme\Item;
use Acme\ItemBuilder;
use Acme\ItemFinder;
use Acme\ItemManager;
use Acme\ItemsSorter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ItemManagerSpec extends ObjectBehavior
{

    public function let(ItemsSorter $sorter, ItemBuilder $itemBuilder, ItemFinder $itemFinder, ImageFinder $imageFinder){
        $this->beConstructedWith($sorter, $itemBuilder, $itemFinder, $imageFinder);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ItemManager::class);
    }

    public function it_sorts_filterd_items_with_images(
        ItemsSorter $sorter,
        ItemBuilder $itemBuilder,
        ItemFinder $itemFinder,
        ImageFinder $imageFinder,
        Item $itemOne,
        Item $itemTwo
    ){
        $itemsData = [
            [
                'name' => 'kot',
                'price' => -10,
                'color' => 'czerwony',
                'weight' => 100,
                'amount' => 1,
            ],
            [
                'name' => 'pies',
                'price' => -10,
                'color' => 'czerwony',
                'weight' => 100,
                'amount' => 1,
            ],
        ];
        $items = [$itemOne, $itemTwo];
        $itemFinder->getAll(Argument::type('array'))->willReturn($itemsData);
        $itemBuilder->buildItem($itemsData[0], new ImagesData([]))->willReturn($itemOne);
        $itemBuilder->buildItem($itemsData[1], new ImagesData([]))->willReturn($itemTwo);
        $imageFinder->getAll()->willReturn([]);
        $sorter->sortByPriceAsc($items)->willReturn($items);

        $this->getItems([])->shouldReturn($items);

    }
}

<?php

namespace spec\Acme;

use Acme\ImageFactory;
use Acme\ImagesData;
use Acme\Item;
use Acme\ItemBuilder;
use PhpSpec\ObjectBehavior;
use Acme\ItemManager;
use Acme\Image;
use Prophecy\Argument;

class ItemBuilderSpec extends ObjectBehavior
{
    public function let(ImageFactory $imageFactory)
    {
        $this->beConstructedWith($imageFactory);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(ItemBuilder::class);
    }

    public function it_builds_item_object(ImageFactory $imageFactory, Image $image, ImagesData $imagesData)
    {
        $itemData = [
            'name' => 'kot',
            'price' => -10,
            'color' => 'czerwony',
            'weight' => 100,
            'amount' => 1,
        ];

        $imageData = [];
        $imagesData->findByItemName('kot')->willReturn($imageData);

        $image->getContent()->willReturn('abc');
        $imageFactory->create($imageData)->willReturn($image);
        /** @var Item $buildedItem */
        $buildedItem = $this->buildItem($itemData, $imagesData);
        $buildedItem->getAmount()->shouldReturn($itemData['amount']);
        $buildedItem->getColor()->shouldReturn($itemData['color']);
        $buildedItem->getWeight()->shouldReturn($itemData['weight']);
        $buildedItem->getPrice()->shouldReturn($itemData['price']);
        $buildedItem->getName()->shouldReturn($itemData['name']);
        $buildedItem->getImageContent()->shouldReturn('abc');
    }


}

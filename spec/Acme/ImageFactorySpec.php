<?php

namespace spec\Acme;

use Acme\Base64Image;
use Acme\DefaultImage;
use Acme\ImageFactory;
use Acme\UrlImage;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ImageFactorySpec extends ObjectBehavior
{
    function it_creates_default_image_on_empty_data()
    {
        $this->create([])->shouldBeLike(new DefaultImage());
    }

    function it_creates_default_image_on_lack_of_data()
    {
        $this->create(['base64' => null, 'url' => null])->shouldBeLike(new DefaultImage());
    }

    function it_creates_base64_image_on_base64_data()
    {
        $this->create(['base64' => '111', 'url' => null])->shouldBeLike(new Base64Image('111'));
    }

    function it_creates_url_image_on_url_data()
    {
        $this->create(['base64' => null, 'url' => 'https'])->shouldBeLike(new UrlImage('https'));
    }
}

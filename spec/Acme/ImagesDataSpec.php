<?php

namespace spec\Acme;

use Acme\ImagesData;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ImagesDataSpec extends ObjectBehavior
{
    function it_finds_image_by_item_name()
    {
        $imagesData = [
            ['itemName' => 'kot']
        ];

        $this->beConstructedWith($imagesData);

        $this->findByItemName('kot')->shouldReturn($imagesData[0]);
    }

    function it_gives_empty_array_if_image_is_not_found()
    {
        $imagesData = [
            ['itemName' => 'kot']
        ];

        $this->beConstructedWith($imagesData);

        $this->findByItemName('pies')->shouldReturn([]);
    }
}

<?php

namespace spec\Acme;

use Acme\Item;
use Acme\ItemsSorter;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class ItemsSorterSpec extends ObjectBehavior
{
    public function it_sorts_items_by_price_asc(Item $item1, Item $item2, Item $item3)
    {
        $item1->getPrice()->willReturn(600);
        $item2->getPrice()->willReturn(100);
        $item3->getPrice()->willReturn(300);

        $items = [$item1, $item2, $item3];
        $this->sortByPriceAsc($items)->shouldBe([
            $item2, $item3, $item1
        ]);
    }
}

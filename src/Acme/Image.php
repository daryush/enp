<?php

namespace Acme;

interface Image
{

    public function getContent();
}

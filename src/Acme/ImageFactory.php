<?php

namespace Acme;

class ImageFactory
{

    public function create(array $data): Image
    {
        if(isset($data['base64']) && null !== $data['base64']){
            return new Base64Image($data['base64']);
        }

        if(isset($data['url']) && null !== $data['url']){
            return new UrlImage($data['url']);
        }

        return new DefaultImage();
    }
}

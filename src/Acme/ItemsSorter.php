<?php

namespace Acme;

class ItemsSorter
{
    public function sortByPriceAsc($items)
    {
        usort($items, function($item1, $item2) {
            if ($item1->getPrice() == $item2->getPrice()) {
                return 0;
            }
            return ($item1->getPrice() < $item2->getPrice()) ? -1 : 1;
        });
        return $items;
    }
}

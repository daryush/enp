<?php

namespace Acme;

use Doctrine\DBAL\Connection;

class ItemFinder
{

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll(array $filters)
    {
        $queryBuilder = $this->connection->createQueryBuilder();
        $queryBuilder
            ->select('i.name', 'i.price', 'i.color', 'i.weight', 'i.amount')
            ->from('items', 'i');

        if ($filters['name'] !== null) {
            $queryBuilder->andWhere('i.name = :name');
            $queryBuilder->setParameter('name', $filters['name']);
        }
        $queryBuilder->andWhere('i.price BETWEEN :minPrice AND :maxPrice');
        $queryBuilder->setParameter('minPrice', $filters['minPrice']);
        $queryBuilder->setParameter('maxPrice', $filters['maxPrice']);

        $queryBuilder->andWhere('i.weight BETWEEN :minWeight AND :maxWeight');
        $queryBuilder->setParameter('minWeight', $filters['minWeight']);
        $queryBuilder->setParameter('maxWeight', $filters['maxWeight']);

        if ($filters['color'] !== null) {
            $queryBuilder->andWhere('i.color = :color');
            $queryBuilder->setParameter('color', $filters['color']);
        }

        if ($filters['coavailableAmountlor'] !== null) {
            $queryBuilder->andWhere('i.availableAmount >= :availableAmount');
            $queryBuilder->setParameter('availableAmount', $filters['availableAmount']);
        }

        return $this->connection->fetchAll($queryBuilder->getSQL(), $queryBuilder->getParameters());
    }
}

<?php

namespace Acme;


class MetaTags
{
    public function getMetaTags($subject)
    {
        $metatags = [
            'title' => 'Generic title',
            'keywords' => 'some, generic, keywords'
        ];

        if ($subject['type'] === 'car') {
            $metatags['title'] = sprintf(
                "Great %s %s form year %s that rides on %s for only %s",
                $subject['Color'],
                $subject['Model'],
                $subject['Year'],
                $subject['Fuel'],
                $subject['Price']
            );

            $metatags['keywords'] = sprintf(
                "%s, %s, %s, %s, %s",
                $subject['Color'],
                $subject['Model'],
                $subject['Year'],
                $subject['Fuel'],
                $subject['Price']
            );
        }

        if ($subject['type'] === 'flat') {
            $metatags['title'] = sprintf(
                "New %s square meters flat on %s floor with view to %s side just for %s",
                $subject['Square'],
                $subject['Floor'],
                $subject['Side'],
                $subject['Price']
            );

            $metatags['keywords'] = sprintf(
                "%s, %s, %s, %s",
                $subject['Square'],
                $subject['Floor'],
                $subject['Side'],
                $subject['Price']
            );
        }

        if ($subject['type'] === 'animal') {
            $metatags['title'] = sprintf(
                "Cute %s, %s, %s years old %s %s for %s",
                $subject['Color'],
                $subject['Size'],
                $subject['HowOld'],
                $subject['Gender'],
                $subject['Genre'],
                $subject['Price']
            );

            $metatags['keywords'] = sprintf(
                "%s, %s, %s, %s, %s, %s",
                $subject['Color'],
                $subject['Size'],
                $subject['HowOld'],
                $subject['Gender'],
                $subject['Genre'],
                $subject['Price']
            );
        }

        return $metatags;
    }
}
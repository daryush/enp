<?php

namespace Acme;

use Doctrine\DBAL\Connection;

class ImageFinder
{

    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    public function getAll()
    {
        return $this->connection->fetchAll('SELECT itemName, url, base64, size FROM images');
    }
}

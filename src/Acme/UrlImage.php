<?php

namespace Acme;


class UrlImage implements Image
{
    /**
     * @var string
     */
    private $url;

    /**
     * UrlImage constructor.
     * @param string $url
     */
    public function __construct($url)
    {
        $this->url = $url;
    }

    public function getContent()
    {
        return file_get_contents($this->url);
    }
}
<?php

namespace Acme;

class ItemManager
{
    /**
     * @var ItemsSorter
     */
    private $sorter;
    /**
     * @var ItemBuilder
     */
    private $itemBuilder;

    /**
     * @var ItemFinder
     */
    private $itemFinder;

    /**
     * @var ImageFinder
     */
    private $imageFinder;

    public function __construct(ItemsSorter $sorter, ItemBuilder $itemBuilder, ItemFinder $itemFinder, ImageFinder $imageFinder)
    {
        $this->sorter = $sorter;
        $this->itemBuilder = $itemBuilder;
        $this->itemFinder = $itemFinder;
        $this->imageFinder = $imageFinder;
    }

    public function getItems(array $criterias)
    {
        $filters = [
            'name' => array_key_exists('name', $criterias) ? $criterias['name'] : null,
            'minPrice' => array_key_exists('minPrice', $criterias) ? $criterias['minPrice'] : 0,
            'maxPrice' => array_key_exists('maxPrice', $criterias) ? $criterias['maxPrice'] : PHP_INT_MAX,
            'color' => array_key_exists('color', $criterias) ? $criterias['color'] : null,
            'minWeight' => array_key_exists('minWeight', $criterias) ? $criterias['minWeight'] : 0,
            'maxWeight' => array_key_exists('maxWeight', $criterias) ? $criterias['maxWeight'] : PHP_INT_MAX,
            'availableAmount' => array_key_exists('availableAmount', $criterias) ? $criterias['availableAmount'] : null,
        ];

        $itemsData = $this->itemFinder->getAll($filters);

        $imagesData = $this->imageFinder->getAll();

        $imagesDataObj = new ImagesData($imagesData);

        $items = [];
        foreach ($itemsData as $itemData) {
            $items[] = $this->itemBuilder->buildItem($itemData, $imagesDataObj);
        }

        return $this->sorter->sortByPriceAsc($items);
    }

    public function applyDiscountToItem($item, $discount)
    {
        $item->setPrice($item->getPrice()*(1-$discount/100));

        return $item;
    }

    public function createGroupedByColor(array $items)
    {
        $groupedItems = [];
        foreach ($items as $item) {
            $groupedItems[$item->getColor()] = $item;
        }

        return $groupedItems;
    }
}

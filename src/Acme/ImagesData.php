<?php

namespace Acme;

class ImagesData
{
    private $imagesData;

    public function __construct(array $imagesData)
    {
        $this->imagesData = $imagesData;
    }

    public function findByItemName(string $itemName)
    {
        foreach ($this->imagesData as $imageData) {
            if ($imageData['itemName'] === $itemName) {
                return $imageData;
            }
        }

        return [];
    }
}

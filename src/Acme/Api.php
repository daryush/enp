<?php

namespace Acme;


use Psr\Log\LoggerInterface;

class Api
{
    /**
     * @var \SoapClient
     */
    private $client;
    private $endpoint;
    private $runtimeMode;
    private $host;
    private $language;
    private $partner;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(
        \SoapClient $client,
        LoggerInterface $logger,
        $endpoint,
        $runtimeMode,
        $host,
        $language,
        $partner
    ) {
        $this->client = $client;
        $this->endpoint = $endpoint;
        $this->runtimeMode = $runtimeMode;
        $this->host = $host;
        $this->language = $language;
        $this->partner = $partner;
        $this->logger = $logger;
    }

    public function fetchUnprocessedOrders(array $ordersData)
    {
        $unprocessedOrderRequestData = [
            'RuntimeMode' => $this->runtimeMode,
            'StorageCode' => $ordersData['storageCode'],
            'RequestUID' => $ordersData['requestUID'],
            'PartnerUrl' => $this->host,
            'LanguageCode' => $this->partner['language'],
            'After' => $ordersData['before'],
            'Before' => $ordersData['after'],
            'API' => false
        ];

        $this->logger->info('Pre soap call');
        $results = $this->client->__soapCall($this->endpoint, $unprocessedOrderRequestData, null, null);
        $this->logger->info('Post soap call');

        foreach ($results as $result) {
            $order = new Order();
            isset($result['price']) ? $order->setPrice($result['price']) : $order->setPrice(0);
            $payment = new Payment();
            $payment->setCustomerEmail($result['buyer']['email']);
            $payment->setCustomerPhone($result['buyer']['phone']);
            $payment->setCustomerFirstName($result['buyer']['firstName']);
            $payment->setCustomerLastName($result['buyer']['lastName']);
            $payment->setShipmentStreet($result['buyer']['delivery->street']);
            $payment->setShipmentPostalCode($result['buyer']['delivery->postalCode']);
            $payment->setShipmentCity($result['buyer']['delivery->city']);
            $payment->setShipmentRecipientName($result['buyer']['delivery->recipientName']);
        }
    }
}
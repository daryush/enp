<?php

namespace Acme;

class ItemBuilder
{
    /**
     * @var ImageFactory
     */
    private $imageFactory;

    public function __construct(ImageFactory $imageFactory)
    {
        $this->imageFactory = $imageFactory;
    }

    public function buildItem(array $itemData, ImagesData $imagesData)
    {
        $item = new Item();
        $item->setName($itemData['name']);
        $item->setPrice($itemData['price']);
        $item->setColor($itemData['color']);
        $item->setWeight($itemData['weight']);
        $item->setAmount($itemData['amount']);

        if ($item->getAmount() > 0) {
            $item->setIsAvailable(true);
        } else {
            $item->setIsAvailable(false);
        }

        $itemImageData = $imagesData->findByItemName($item->getName());
        $item->setImage($this->imageFactory->create($itemImageData));

        return $item;
    }
}

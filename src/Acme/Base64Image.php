<?php

namespace Acme;

class Base64Image implements Image
{

    /**
     * @var
     */
    private $base64;

    public function __construct(string $base64)
    {
        $this->base64 = $base64;
    }

    public function getContent()
    {
        return base64_decode($this->base64);
    }
}